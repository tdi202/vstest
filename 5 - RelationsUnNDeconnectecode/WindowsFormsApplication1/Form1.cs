﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        DataSet ds = new DataSet();
        SqlConnection cn = new SqlConnection(@"Data source=.\testExpress;initial catalog=librairie;user id=sa;password=123456");

        SqlCommand comCla = new SqlCommand();
        SqlCommand comEdi = new SqlCommand();
        SqlCommand comOuv = new SqlCommand();

        SqlDataAdapter daCla;
        SqlDataAdapter daEdi;
        SqlDataAdapter daOuv;

        BindingSource bsCla = new BindingSource();
        BindingSource bsEdi = new BindingSource();
        BindingSource bsOuv = new BindingSource();

        SqlCommandBuilder cb;


        private void Form1_Load(object sender, EventArgs e)
        {
            cn.Open();
            comCla.CommandText = "select * from classification";
            comEdi.CommandText = "select * from editeur";
            comOuv.CommandText = "select * from ouvrage";
            comCla.Connection = cn;
            comEdi.Connection = cn;
            comOuv.Connection = cn;

            daCla = new SqlDataAdapter(comCla);
            daEdi = new SqlDataAdapter(comEdi);
            daOuv = new SqlDataAdapter(comOuv);

            daCla.Fill(ds, "Classification");
            daEdi.Fill(ds, "Editeur");
            daOuv.Fill(ds, "Ouvrage");

            DataColumn c1 = ds.Tables["Classification"].Columns["numrub"];
            DataColumn c2 = ds.Tables["Ouvrage"].Columns["numrub"];

            DataColumn c3 = ds.Tables["Editeur"].Columns["nomed"];
            DataColumn c4 = ds.Tables["Ouvrage"].Columns["nomed"];


            DataRelation r1 = new DataRelation("fk_ouvrage_classification", c1, c2);
            DataRelation r2 = new DataRelation("fk_ouvrage_editeur", c3, c4);


            ds.Relations.Add(r1);
            ds.Relations.Add(r2);

            //Repmlir le combo des classifications
            bsCla.DataSource = ds;
            bsCla.DataMember = "Classification";
            comboBox1.DataSource = bsCla;
            comboBox1.DisplayMember = "LibRub";
            comboBox1.ValueMember = "numrub";


            //Repmlir le combo des editeur
            bsEdi.DataSource = ds;
            bsEdi.DataMember = "Editeur";
            comboBox2.DataSource = bsEdi;
            comboBox2.DisplayMember = "NomEd";
            comboBox2.ValueMember = "NomEd";

            
            
            //remplir la liste avec les ouvrages de la classification selectionne
            bsOuv.DataSource = bsCla;
            bsOuv.DataMember = "fk_ouvrage_classification";
            listBox1.DataSource = bsOuv;
            listBox1.DisplayMember = "NomOuvr";
            listBox1.ValueMember = "Numouvr";


            //afficher les zones de textes
            textBox1.DataBindings.Add("text", bsOuv, "numouvr");
            textBox2.DataBindings.Add("text", bsOuv, "nomouvr");
            textBox3.DataBindings.Add("text", bsOuv, "anneeparu");

            comboBox2.DataBindings.Add("selectedvalue", bsOuv, "nomed");



        }
    }
}
